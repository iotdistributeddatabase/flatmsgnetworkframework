/*
 * Msg
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <MessageSink.hpp>

namespace Msg
{

MessageSink::MessageSink(QTcpSocket& sock,
                         std::function<void (MessageBuffer&&)> receiveMsg)
    : sock_(sock), size_(0), receiveMsg_(receiveMsg), msgSizeToRead_(0)
{
}

void MessageSink::send(IMessageBuilder& msg)
{
    qint64 writeLen = sock_.write(reinterpret_cast<char*>(msg.getBufferPtr()),
                                  static_cast<qint64>(msg.getSize()));
    qDebug() << "MessageSink::sendMsg size=" <<  msg.getSize()
             << "writeLen=" << writeLen
             << "sock=" << sock_.socketDescriptor();
    if(writeLen != static_cast<qint64>(msg.getSize()) or not sock_.flush())
    {
        qWarning() << "MessageSink could not send whole message with size=" << msg.getSize()
                   << " error " <<  sock_.errorString();
    }
}

void MessageSink::receive()
{
    while (sock_.bytesAvailable())
    {
        if(not msgSizeToRead_)
        {
            qint64 readLen = sock_.read(reinterpret_cast<char*>(&size_), sizeof(size_));
            if(readLen != static_cast<qint64>(sizeof(size_)))
            {
                qWarning() << "MessageSink could not receive message size"
                           << " error " <<  sock_.errorString();
                return;
            }
            msgSizeToRead_ = ::flatbuffers::ReadScalar<::flatbuffers::uoffset_t>(&size_);
            if(msgSizeToRead_ <= 0)
            {
                qWarning() << "MessageSink msg size to read is invalid"
                           << " error " <<  sock_.errorString();
                return;
            }
            MessageBuffer msgBuffer {msgSizeToRead_};
            msgBuffer_ = std::move(msgBuffer);

            qDebug() << "MessageSink::receive msg with size=" << msgSizeToRead_
                     << "sock=" << sock_.socketDescriptor();
        }

        qint64 readOffset = msgBuffer_.getSize() - msgSizeToRead_;
        qint64 readLen = sock_.read(msgBuffer_.getMsgBufPtr() + readOffset, msgSizeToRead_);
        if(readLen <= 0)
        {
            qWarning() << "MessageSink could not receive message with size=" << msgBuffer_.getSize()
                       << " error " <<  sock_.errorString();
            return;
        }
        msgSizeToRead_ -= readLen;

        qDebug() << "MessageSink::receive readLen=" << readLen
                 << "sock=" << sock_.socketDescriptor();

        if(not msgSizeToRead_)
        {
            receiveMsg_(std::move(msgBuffer_));
        }
    }
}

} // namespace Msg

