/*
 * Msg
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <cstdint>
#include <memory>

#include <MessageBuffer.hpp>

namespace Msg
{

MessageBuffer::MessageBuffer(MessageBuffer&& r)
    : msgBuf_(r.msgBuf_.release()), size_(std::exchange(r.size_,0))
{
}

MessageBuffer::MessageBuffer()
    : msgBuf_(nullptr), size_(0)
{

}

MessageBuffer &MessageBuffer::operator=(MessageBuffer&& r)
{
    if(this != &r)
    {
        msgBuf_.reset(r.msgBuf_.release());
        size_ = r.size_;
        r.size_ = 0;
    }
    return *this;
}

MessageBuffer::MessageBuffer(const qint64 size)
    : msgBuf_(std::make_unique<char[]>(static_cast<std::size_t>(size))),
      size_(size)
{
}

char* MessageBuffer::getMsgBufPtr() const
{
    return msgBuf_.get();
}

qint64 MessageBuffer::getSize() const
{
    return size_;
}

} // namespace Msg
