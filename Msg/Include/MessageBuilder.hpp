/*
 * Msg
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MSG_MESSAGEBUILDER_HPP
#define MSG_MESSAGEBUILDER_HPP

#include "IMessageBuilder.hpp"
#include <flatbuffers/flatbuffers.h>
#include <cstdint>
#include <cstddef>

namespace Msg
{

class MessageBuilder : public Msg::IMessageBuilder
{
public:
    MessageBuilder() = default;
    MessageBuilder(const MessageBuilder&) = delete;
    MessageBuilder& operator=(const MessageBuilder&) = delete;
    MessageBuilder(MessageBuilder&&) = delete;
    MessageBuilder& operator=(MessageBuilder&&) = delete;
    uint8_t* getBufferPtr() override
    {
        return builder_.GetBufferPointer();
    }
    std::size_t getSize() override
    {
        return builder_.GetSize();
    }
    ~MessageBuilder() override = default;

protected:
    template<typename... Ts>
    auto createString(Ts... args);

    template<typename... Ts>
    auto createVectorOfStrings(Ts... args);

    template<typename... Ts>
    auto createVector(Ts... args);

    template<typename T>
    auto createStruct(const T& structObj);

    template<typename FUN, typename... Ts>
    auto createField(FUN fun, Ts... args);

    template<typename CreateMsgFun, typename T, typename Msg>
    void createAnyMsg(CreateMsgFun createMsgFun, T type, Msg msg);

    template<typename T, typename FUN, typename... Ts>
    void createMsg(T type, FUN fun, Ts... args);

protected:
    flatbuffers::FlatBufferBuilder builder_;
};

template<typename... Ts>
auto MessageBuilder::createString(Ts... args)
{
    return builder_.CreateString(args...);
}

template<typename... Ts>
auto MessageBuilder::createVectorOfStrings(Ts... args)
{
    return builder_.CreateVectorOfStrings(args...);
}

template<typename... Ts>
auto MessageBuilder::createVector(Ts... args)
{
    return builder_.CreateVector(args...);
}

template<typename T>
auto MessageBuilder::createStruct(const T& structObj)
{
    return builder_.CreateStruct(structObj);
}

template<typename FUN, typename... Ts>
auto MessageBuilder::createField(FUN fun, Ts... args)
{
    return fun(builder_, args...);
}

template<typename CreateMsgFun, typename T, typename Msg>
void MessageBuilder::createAnyMsg(CreateMsgFun createMsgFun, T type, Msg msg)
{
    auto msgBuild = createMsgFun(builder_, type, msg.Union());
    builder_.FinishSizePrefixed(msgBuild);
}

} // namespace Msg

#endif // MSG_MESSAGEBUILDER_HPP
