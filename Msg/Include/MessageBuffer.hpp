/*
 * Msg
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MSG_MSGBUFFER_HPP
#define MSG_MSGBUFFER_HPP

#include <cstdint>
#include <memory>
#include <QtGlobal>

namespace Msg
{

class MessageBuffer
{
public:
    MessageBuffer();
    MessageBuffer(const MessageBuffer&) = delete;
    MessageBuffer& operator=(const MessageBuffer&) = delete;
    MessageBuffer(MessageBuffer&& r);
    MessageBuffer& operator=(MessageBuffer&& r);

    MessageBuffer(const qint64 size);
    char* getMsgBufPtr() const;
    qint64 getSize() const;

private:
    std::unique_ptr<char[]> msgBuf_;
    qint64 size_;
};

} // namespace Msg

#endif // MSG_MSGBUFFER_HPP
