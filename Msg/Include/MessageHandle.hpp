/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MSG_MESSAGEHANDLE_HPP
#define MSG_MESSAGEHANDLE_HPP

#include "MessageBuffer.hpp"

namespace Msg
{

template <typename T>
class MessageHandle
{
public:
    MessageHandle(MessageBuffer&& msgBuff, const T* msg)
        : msgBuffer_(std::move(msgBuff)), msg_(msg)
    {
    }
    MessageHandle(const MessageHandle&) = delete;
    MessageHandle<T>& operator=(const MessageHandle&) = delete;
    MessageHandle(MessageHandle&&) = default;
    MessageHandle<T>& operator=(MessageHandle&&) = default;

    const T& operator*() const
    {
        return *msg_;
    }

    const T* operator->() const
    {
        return msg_;
    }

    operator bool() const
    {
        return msg_;
    }

private:
    MessageBuffer msgBuffer_;
    const T* msg_;
};

} // namespace Msg

#endif // MSG_MESSAGEHANDLE_HPP
