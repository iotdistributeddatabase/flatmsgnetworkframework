/*
 * Msg
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MSG_MESSAGESINK_HPP
#define MSG_MESSAGESINK_HPP

#include "MessageBuffer.hpp"
#include <IMessageBuilder.hpp>
#include <flatbuffers/flatbuffers.h>
#include <memory>
#include <QTcpSocket>
#include <QDebug>

namespace Msg
{

class MessageSink
{
public:
    explicit MessageSink(QTcpSocket& sock,
                         std::function<void(MessageBuffer&&)> receiveMsg);
    void send(IMessageBuilder& msg);
    void receive();
    void close();

private:
    QTcpSocket& sock_;
    ::flatbuffers::uoffset_t size_;
    std::function<void(MessageBuffer)> receiveMsg_;
    qint64 msgSizeToRead_;
    MessageBuffer msgBuffer_;
};

} // namespace Msg

#endif // MSG_MESSAGESINK_HPP
