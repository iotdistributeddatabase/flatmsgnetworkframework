/*
 * Network
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ConnectionManager.hpp"

namespace Network
{

ConnectionManager::ConnectionManager(const qintptr socketDescriptor,
                                     const MessageDispatcher& dispatch)
    : socketDescriptor_(socketDescriptor), dispatch_(dispatch)
{

}

ConnectionManager::~ConnectionManager()
{

}

void ConnectionManager::doWork()
{
    socket_ = std::make_unique<QTcpSocket>();
    if(socket_->setSocketDescriptor(socketDescriptor_))
    {
        auto handler = std::bind(&ConnectionManager::receiveMsg,
                                 this, std::placeholders::_1);
        sink_ = std::make_unique<Msg::MessageSink>(*socket_, handler);
        connect(socket_.get(), SIGNAL(connected()),
                this, SLOT(onConnected()));
        connect(socket_.get(), SIGNAL(disconnected()),
                this, SLOT(onDisconnect()));
        connect(socket_.get(), SIGNAL(readyRead()),
                this, SLOT(onReadyRead()));
    }
    else
    {
        quit();
    }
}

void ConnectionManager::onConnected()
{
    qDebug() << "Connected socketDescriptor =" << socketDescriptor_;
}

void ConnectionManager::onDisconnect()
{
    qDebug() << "Disconnected socketDescriptor =" << socketDescriptor_;
    quit();
}

void ConnectionManager::onReadyRead()
{
    qDebug() << "onReadyRead recived bytes =" << socket_->bytesAvailable();
    sink_->receive();
}

void ConnectionManager::receiveMsg(Msg::MessageBuffer &&msgBuf)
{
    dispatch_(std::move(msgBuf), this);
}

void ConnectionManager::doWrite(IMsgPtr msg)
{
    sink_->send(*msg);
}

void ConnectionManager::quit()
{
    emit finished(this);
}

} // namespace Network
