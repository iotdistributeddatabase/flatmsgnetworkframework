/*
 * Network
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NETWORK_CONNECTIONMANAGER_HPP
#define NETWORK_CONNECTIONMANAGER_HPP

#include <TcpServer.hpp>
#include <Worker.hpp>
#include <MessageTypes.hpp>
#include <MessageBuffer.hpp>
#include <MessageSink.hpp>
#include <QTcpSocket>
#include <QObject>
#include <memory>
#include <functional>

namespace Network
{

class ConnectionManager : public QObject, public Parallel::Worker
{
    Q_OBJECT
    Q_INTERFACES(Parallel::Worker)
public:
    explicit ConnectionManager(const qintptr socketDescriptor,
                               const MessageDispatcher& dispatch);
    ~ConnectionManager() override;

signals:
    void finished(const Parallel::Worker*) override;

public slots:
    void doWork() override;
    void quit() override;
    void onConnected();
    void onDisconnect();
    void onReadyRead();
    void doWrite(IMsgPtr msg);

private:
    void receiveMsg(Msg::MessageBuffer&& msgBuf);

private:
    qintptr socketDescriptor_;
    std::unique_ptr<QTcpSocket> socket_;
    std::unique_ptr<Msg::MessageSink> sink_;
    const MessageDispatcher dispatch_;
};

} // namespace Network

#endif // NETWORK_CONNECTIONMANAGER_HPP
