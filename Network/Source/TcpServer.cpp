/*
 * Network
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <TcpServer.hpp>
#include "ConnectionManager.hpp"
#include <memory>
#include <QObject>

namespace Network
{

TcpServer::TcpServer(const QHostAddress &address, const quint16 port,
                     const std::size_t max_threads)
    : address_(address), port_(port), workerPool_(max_threads)
{
}

TcpServer::~TcpServer()
{
}

bool TcpServer::startTcpServer()
{
    if(this->listen(address_, port_))
    {
        return true;
    }
    return false;
}

bool TcpServer::stopTcpServer()
{
    this->close();
    return true;
}

void TcpServer::incomingConnection(qintptr socketDescriptor)
{
    qDebug() << "Incoming connection socketDescriptor =" << socketDescriptor;

    std::unique_ptr<ConnectionManager> worker
            = std::make_unique<ConnectionManager>(socketDescriptor, createMessageDispatcher());
    workerPool_.addWorker(std::move(worker));
}

} // namespace Network
