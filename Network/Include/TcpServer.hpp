/*
 * Network
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NETWORK_TCPSERVER_HPP
#define NETWORK_TCPSERVER_HPP

#include <QTcpServer>
#include <WorkerPool.hpp>
#include "ConnectionManagerTypes.hpp"

namespace Network
{

class TcpServer : public QTcpServer
{
public:
    explicit TcpServer(const QHostAddress &address, const quint16 port,
                       const std::size_t max_threads);
    ~TcpServer() override;

protected:
    bool startTcpServer();
    bool stopTcpServer();
    void incomingConnection(qintptr socketDescriptor) override;
    virtual MessageDispatcher createMessageDispatcher() = 0;

protected:
    const QHostAddress address_;
    const quint16 port_;
    Parallel::WorkerPool workerPool_;
};

} // namespace Network

#endif // NETWORK_TCPSERVER_HPP
