/*
 * Parallel
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef WORKERPOOL_HPP
#define WORKERPOOL_HPP

#include <vector>
#include <WorkerController.hpp>

namespace Parallel
{

class WorkerPool
{
public:
    WorkerPool(std::size_t maxWorkers);
    WorkerPool(const WorkerPool&) = delete;
    WorkerPool& operator=(const WorkerPool&) = delete;

    template<typename WorkerType>
    void addWorker(std::unique_ptr<WorkerType> worker)
    {
        workers_[next_].addWorker(std::move(worker));
        ++next_;
        if(next_ == workers_.size())
        {
            next_ = 0;
        }
    }

protected:
    std::size_t next_;
    std::vector<WorkerController> workers_;
};

} // namespace Parallel

#endif // WORKERPOOL_HPP
