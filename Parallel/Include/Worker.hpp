/*
 * Parallel
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef WORKER_HPP
#define WORKER_HPP

#include <QtPlugin>

namespace Parallel
{

class Worker
{
public:
    Worker() = default;
    Worker(const Worker&) = delete;
    Worker& operator=(const Worker&) = delete;
    virtual ~Worker(){}

signals:
    virtual void finished(const Worker*) = 0;

public slots:
    /* ... here is the expensive or blocking operation ... */
    virtual void doWork() = 0;
    virtual void quit() = 0;
};

} // namespace Parallel

Q_DECLARE_INTERFACE(Parallel::Worker, "Parallel::Worker")

#endif // WORKER_HPP
