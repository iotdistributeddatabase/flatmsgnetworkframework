/*
 * Parallel
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef WORKERCONTROLLER_HPP
#define WORKERCONTROLLER_HPP

#include <QThread>
#include <Worker.hpp>
#include <memory>
#include <unordered_map>

namespace Parallel
{

class WorkerController
{
    QThread workerThread;
public:
    explicit WorkerController();
    WorkerController(const WorkerController&) = delete;
    WorkerController& operator=(const WorkerController&) = delete;
    ~WorkerController();

    template<typename WorkerType>
    void addWorker(std::unique_ptr<WorkerType> worker)
    {
        worker->moveToThread(&workerThread);
        QObject::connect(&workerThread, &QThread::finished, worker.get(), &QObject::deleteLater);
        QObject::connect(worker.get(), SIGNAL(finished(const Parallel::Worker*)),
                         worker.get(), SLOT(deleteLater()), Qt::QueuedConnection);
        QMetaObject::invokeMethod(worker.get(), "doWork", Qt::QueuedConnection);
        worker.release();
        //QObjectGaurd tmp(worker_, [](QObject* ptr){ ptr->deleteLater(); });
        //workers_.insert(std::make_pair(worker.release(), std::move(tmp)));

    }

protected:
    //using QObjectGaurd = std::unique_ptr<QObject, void (*)(QObject*)>;
    //std::unordered_map<Worker*, QObjectGaurd> workers_;
};

} // namespace Parallel

#endif // WORKERCONTROLLER_HPP
